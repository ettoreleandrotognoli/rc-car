#ifndef __TWO_WHEEL_VEHICLE__
#define __TWO_WHEEL_VEHICLE__

#include "IO.h"
#include "Vehicle.h"


class TwoWheelVehicle : public Vehicle{
protected:
    BridgeHController& leftWheel;
    BridgeHController& rightWheel;
    double speed;
    double direction;

    void update();

public:
    TwoWheelVehicle(
        BridgeHController& leftWheel,
        BridgeHController& rightWheel
    );
    void setSpeed(double speed);
    void setDirection(double direction);
    void setState(const vehicle_state_t& state);

};

#endif