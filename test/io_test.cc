#include "IO.h"
#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <iostream>


class AnologOutputMock : public  AnalogOutput {
public:
  MOCK_METHOD1(write, void(double value));
};



TEST(BridgeHController,forward){
  AnologOutputMock forward;
  AnologOutputMock backward;

  EXPECT_CALL(forward, write(1))
    .Times(1);
  EXPECT_CALL(backward, write(0))
    .Times(1);


  BridgeHController controller(
      forward, backward
  );
  controller.forward(1);
}


TEST(BridgeHController,backward){
  AnologOutputMock forward;
  AnologOutputMock backward;

  EXPECT_CALL(backward, write(1))
    .Times(1);
  EXPECT_CALL(forward, write(0))
    .Times(1);


  BridgeHController controller(
      forward, backward
  );
  controller.backward(1);
}


TEST(BridgeHController,stop){
  AnologOutputMock forward;
  AnologOutputMock backward;

  EXPECT_CALL(backward, write(0))
    .Times(1);
  EXPECT_CALL(forward, write(0))
    .Times(1);


  BridgeHController controller(
      forward, backward
  );
  controller.stop();
}



int main(int argc,char ** argv){
  ::testing::InitGoogleMock(&argc, argv);
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
