#ifndef __IO__ARDUINOIO__
#define __IO__ARDUINOIO__

#ifdef __AVR__
#include <Arduino.h>
#include "../IO.h"

class ArduinoAnalogOutput : public AnalogOutput{
protected:
    unsigned char pinNumber;
public:
    ArduinoAnalogOutput(unsigned char pinNumber);
    void write(double value);
};

class ArduinoIO {
public:
    ArduinoIO();
    ArduinoAnalogOutput analogOutput(unsigned char pinNumber);
};

extern ArduinoIO IO;

#endif
#endif