#include <IO.h>
#include <Vehicle.h>
#include <BridgeHVehicle.h>

#include <BluetoothSerial.h>


auto gpio34 = IO.analogOutput(14);
auto gpio35 = IO.analogOutput(12);

auto gpio25 = IO.analogOutput(25);
auto gpio26 = IO.analogOutput(26);

BridgeHController engine(gpio34,gpio35);
BridgeHController axis(gpio25, gpio26);

BluetoothSerial SerialBT;
BridgeHVechile vehicle(engine, axis);

VehicleParser parser(vehicle, SerialBT);

void setup() {  
  Serial.begin(115200);
  SerialBT.begin("simple-vehicle");  
}

void loop() {
  parser.parseStream();
}