FROM fedora

RUN dnf install -y @development-tools
RUN dnf install -y gcc-c++ libasan liblsan lcov
RUN dnf install -y gmock gmock-devel gtest gtest-devel
