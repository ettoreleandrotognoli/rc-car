#include <IO.h>
#include <Vehicle.h>

#include <BluetoothSerial.h>

BluetoothSerial SerialBT;

DebugVehicle vehicle(Serial);
VehicleParser parser(vehicle, SerialBT);

void setup() {  
  Serial.begin(115200);
  SerialBT.begin("simple-vehicle");  
}

void loop() {
  parser.parseStream();
}