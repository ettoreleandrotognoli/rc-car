#ifndef __IO_H__
#define __IO_H__

class AnalogOutput {
public:
    virtual void write(double value) = 0;
};

class BridgeHController {
protected:
    AnalogOutput& forwardPin;
    AnalogOutput& backwardPin;
public:
    BridgeHController(
        AnalogOutput& forwardPin,
        AnalogOutput& backwardPin
    );

    void forward(double value);

    void backward(double value);

    void stop();

    void setPower(double value);
};

#endif

#ifdef __AVR__
#include "io/ArduinoIO.h"
#endif
#ifdef ESP32
#include "io/ESP32IO.h"
#endif

