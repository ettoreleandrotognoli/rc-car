#include <IO.h>
#include <Vehicle.h>
#include <TwoWheelVehicle.h>

#include <BluetoothSerial.h>


auto gpio34 = IO.analogOutput(14);
auto gpio35 = IO.analogOutput(12);

auto gpio25 = IO.analogOutput(25);
auto gpio26 = IO.analogOutput(26);

BridgeHController left(gpio34,gpio35);
BridgeHController right(gpio25, gpio26);

BluetoothSerial SerialBT;
TwoWheelVehicle vehicle(left, right);

VehicleParser parser(vehicle, SerialBT);

void setup() {  
  Serial.begin(115200);
  SerialBT.begin("simple-2w-vehicle");  
}

void loop() {
  parser.parseStream();
}