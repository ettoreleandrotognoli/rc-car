#include "ArduinoIO.h"

#ifdef __AVR__


ArduinoAnalogOutput::ArduinoAnalogOutput(unsigned char pinNumber) {
  this->pinNumber = pinNumber;
  pinMode(this->pinNumber,OUTPUT);
}

void ArduinoAnalogOutput::write(double value){
  analogWrite(this->pinNumber, value * 255);
}

ArduinoIO::ArduinoIO() {

}

ArduinoAnalogOutput ArduinoIO::analogOutput(unsigned char pinNumber){
  return ArduinoAnalogOutput(pinNumber);
}

ArduinoIO IO;

#endif