#include "IO.h"
#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <iostream>

#include <Vehicle.h>

TEST(unserialize_vehicle_state,unserialize_05_05){
    char serialized[8] = {
        0x00,0x00,0x00,0x3f,
        0x00,0x00,0x00,0x3f
    };
    vehicle_state_t state = unserialize_vehicle_state(serialized);
    EXPECT_EQ(state.speed,0.5);
    EXPECT_EQ(state.direction,0.5);
}


int main(int argc,char ** argv){
  ::testing::InitGoogleMock(&argc, argv);
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
