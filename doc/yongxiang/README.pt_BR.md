# YONGXIANG

YONGXIANG
Radio Control Car

## Custo

Data | Local | Preço | Referência
-----|-------|-------|------------|
25/02/2019 | Marília - SP BR | R$50,00 |

## Dispositivo

![](./assets/0001.jpg)
![](./assets/0002.jpg)

## Desmontagem

### Controle

CI - TX2S GA6659

![](./assets/0003.jpg)
![](./assets/0004.jpg)
![](./assets/0005.jpg)

### Veículo

CI - FMRX2AMS CG36SP 18

![](./assets/0006.jpg)
![](./assets/0007.jpg)

# Componentes Extras

## Bateria 18650
### Custo

Data | Local | Preço | Referência |
-----|-------|-------|------------|
08/04/2019 | Marília - SP BR | R$6,00 |

## Suporte Bateria

![](https://http2.mlstatic.com/2x-suporte-para-duas-baterias-18650-litio-case-adaptador-D_NQ_NP_767765-MLB25997222805_092017-O.webp)

### Custo

Data | Local | Preço | Referência |
-----|-------|-------|------------|
08/04/2019 | Marília - SP BR | R$8,35 | [MercadoLivre](https://produto.mercadolivre.com.br/MLB-911947833-2x-suporte-para-duas-baterias-18650-litio-case-adaptador-_JM?quantity=1)

## Fonte

![](https://http2.mlstatic.com/xl6009-conversor-dc-dc-step-up-boost-regulador-tenso-D_NQ_NP_381315-MLB25217943428_122016-O.webp)

### Custo

Data | Local | Preço | Referência |
-----|-------|-------|------------|
08/04/2019 | Marília - SP BR | R$14,80 | [MercadoLivre](https://produto.mercadolivre.com.br/MLB-819773399-xl6009-conversor-dc-dc-step-up-boost-regulador-tenso-_JM?quantity=1&variation=18583346454)

## Ponte H

![](https://http2.mlstatic.com/ponte-h-l298-driver-motor-passo-cc-modulo-para-arduino-pic-D_NQ_NP_184221-MLB20738166071_052016-O.webp)

### Custo

Data | Local | Preço | Referência |
-----|-------|-------|------------|
08/04/2019 | Marília - SP BR | R$24,90 | [MercadoLivre](https://produto.mercadolivre.com.br/MLB-767222140-ponte-h-l298-driver-motor-passo-cc-modulo-para-arduino-pic-_JM?quantity=1)