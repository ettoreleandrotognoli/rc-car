
#include "TwoWheelVehicle.h"
#include <math.h> 

#define PI 3.14159265

TwoWheelVehicle::TwoWheelVehicle(
    BridgeHController& leftWheel,
    BridgeHController& rightWheel
) : leftWheel(leftWheel), rightWheel(rightWheel) {
    this->speed = 0;
    this->direction = 0;
}

void TwoWheelVehicle::setState(const vehicle_state_t& state) {
    Vehicle::setState(state);
    this->update();
}


void TwoWheelVehicle::setSpeed(double speed) {
  this->speed = speed;
}

void TwoWheelVehicle::setDirection(double direction) {
    this->direction = direction;
}

void TwoWheelVehicle::update() {
    double speed = this->speed * (1 + cos(this->direction * PI)/4);
    double direction = this->direction * (1 + cos(this->speed * PI)/4);
    double power = sqrt(direction * direction + speed * speed);
    double rad = atan2(speed, direction);
    double front = sin(rad);
    double axis = cos(rad);
    double left = (front + axis) * power;
    double right = (front - axis) * power;
    this->leftWheel.setPower(left);
    this->rightWheel.setPower(right);
}