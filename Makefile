CC:=g++
RUN:=
DIR := $(CURDIR)
UID := $(id -u $USER)
OBJSUFFIX := cpp
OBJHSUFFIX := h

RUNSUFFIX := cc
RUNHSUFFIX := hh

ARDUINOSUFFIX := ino

CFLAGS := -pipe -g -std=c++11
CLIBS := -L/usr/lib64 -L/usr/lib -lpthread 
CTEST_LIBS = -lgtest -lgmock

CINCLUDES := -I/usr/include
CINCLUDES := -I$(DIR)/src -I$(DIR)/linux $(CINCLUDES)

OSRC := $(shell find -type f -iname '*.$(OBJSUFFIX)' )
OHSRC := $(shell find -type f -iname '*.$(OBJHSUFFIX)' )
OBJECTS := $(foreach x, $(basename $(OSRC)), $(x).o)

RSRC := $(shell find -type f -iname '*.$(RUNSUFFIX)' )
RHSRC := $(shell find -type f -iname '*.$(RUNHSUFFIX)' )
RUNS := $(foreach x, $(basename $(RSRC)), $(x).run)
TESTS := $(foreach x, $(basename $(RUNS)), $(x).test)

INOSRC := $(shell find -type f -iname '*.$(ARDUINOSUFFIX)' )
INOOBJ := $(foreach x, $(basename $(INOSRC)), $(x).ao)

docker-make:
	docker run --rm -v $(PWD):$(PWD) -w $(PWD) -t rccar-gcc make test

test: $(TESTS)
	lcov -c --no-external -d $(CURDIR) -b $(CURDIR) --output-file main_coverage.info
	genhtml main_coverage.info --output-directory out

all: $(OBJECTS) $(RUNS)

ino: $(INOOBJ)

clean:
	rm -f $(OBJECTS) $(RUNS) $(TESTS) $(INOOBJ)
	rm -f $(shell find -type f -iname '*.gcda' ) $(shell find -type f -iname '*.gcno' ) $(shell find -type f -iname '*.info' )

%.o: %.cpp $(OHSRC)
	$(CC) $(CFLAGS) -fprofile-arcs -ftest-coverage -c $< -o $@ $(CINCLUDES)

%.ao: %.ino $(OBJECTS)
	cp -f linux/template $<.cxx
	cat $< >> $<.cxx
	$(CC) $(CFLAGS) $<.cxx -o $@ $(CLIBS) $(CINCLUDES) $(OBJECTS)
	rm -f $<.cxx

%.run: %.cc $(OBJECTS)
	$(export GCOV_PREFIX="$(CURDIR)/coverage")
	$(CC) $(CFLAGS) -fprofile-arcs -ftest-coverage $< -o $@ $(CLIBS) $(CTEST_LIBS) $(CINCLUDES) $(OBJECTS)

%.test: %.run
	$(RUN) $< > $@

init:
	docker build -t rccar-gcc - < Dockerfile
