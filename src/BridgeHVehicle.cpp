
#include "BridgeHVehicle.h"

BridgeHVechile::BridgeHVechile(
    BridgeHController& engine,
    BridgeHController& frontAxis
) : engine(engine), frontAxis(frontAxis) {

}


void BridgeHVechile::setSpeed(double speed) {
    if(speed == 0){
        this->engine.stop();
    }
    else if( speed > 0){
        this->engine.forward(speed);
    }
    else {
        this->engine.backward(-speed);
    }
}

void BridgeHVechile::setDirection(double direction) {
    if(direction == 0){
        this->frontAxis.stop();
    }
    else if( direction > 0){
        this->frontAxis.forward(direction);
    }
    else {
        this->frontAxis.backward(-direction);
    }
}