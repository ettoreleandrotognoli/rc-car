#ifndef __BRIDGE_H_VEHICLE__
#define __BRIDGE_H_VEHICLE__

#include "IO.h"
#include "Vehicle.h"

class BridgeHVechile : public Vehicle{
protected:
    BridgeHController& engine;
    BridgeHController& frontAxis;
public:
    BridgeHVechile(
        BridgeHController& engine,
        BridgeHController& frontAxis
    );
    void setSpeed(double speed);
    void setDirection(double direction);
};

#endif
