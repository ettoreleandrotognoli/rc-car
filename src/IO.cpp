#include "IO.h"

BridgeHController::BridgeHController(
    AnalogOutput &forwardPin,
    AnalogOutput &backwardPin) : forwardPin(forwardPin), backwardPin(backwardPin){
      this->stop();
}

void BridgeHController::forward(double value){
  this->backwardPin.write(0);
  this->forwardPin.write(value);
}

void BridgeHController::backward(double value){
  this->forwardPin.write(0);
  this->backwardPin.write(value);
}

void BridgeHController::stop(){
  this->forwardPin.write(0);
  this->backwardPin.write(0);
}

void BridgeHController::setPower(double value) {
  if(value == 0) {
    this->stop();
  }
  else if (value > 0) {
    this->forward(value);
  }
  else {
    this->backward(abs(value));
  }
}