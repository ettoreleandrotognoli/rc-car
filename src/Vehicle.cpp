
#include "Vehicle.h"

vehicle_state_t unserialize_vehicle_state(void* buffer){
    return *(vehicle_state_t*)buffer;
}

void Vehicle::setState(const vehicle_state_t& state) {
    this->setSpeed(state.speed);
    this->setDirection(state.direction);
}

DebugVehicle::DebugVehicle(Print& printer):
    printer(printer){
}

void DebugVehicle::setSpeed(double speed) {
    this->printer.print("Speed: ");
    this->printer.println(speed);
}
void DebugVehicle::setDirection(double direction) {
    this->printer.print("Direction: ");
    this->printer.println(direction);
}

VehicleParser::VehicleParser(Vehicle& vehicle, Stream& stream):
    vehicle(vehicle),
    stream(stream){

}

void VehicleParser::parseStream(){
    vehicle_state_t buffer;

    if(!this->stream.available()){
        return;
    }
    this->stream.readBytes((char*)(void*)&buffer, sizeof(vehicle_state_t));
    this->vehicle.setState(buffer);
}