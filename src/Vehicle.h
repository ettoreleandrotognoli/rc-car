#ifndef __VEHICLE__
#define __VEHICLE__

#include "Arduino.h"

typedef struct vehicle_state {
    float speed;
    float direction;
} vehicle_state_t;

vehicle_state_t unserialize_vehicle_state(void* buffer);

class Vehicle {
public:
    virtual void setSpeed(double speed) = 0;
    virtual void setDirection(double direction) = 0;
    virtual void setState(const vehicle_state_t& state);
};

class DebugVehicle: public Vehicle {
protected:
    Print& printer;
public:
    DebugVehicle(Print& printer);
    void setSpeed(double speed);
    void setDirection(double direction);
};

class VehicleParser{
protected:
    Vehicle& vehicle;
    Stream& stream;
public:
    VehicleParser(Vehicle& vehicle, Stream& stream);
    void parseStream();
};

#endif